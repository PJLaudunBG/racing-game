
import React from 'react';
import Grid from '@material-ui/core/Grid';

import Aux from '../../hoc/HOC';
import NavContainer from '../Navbar/NavbarContianer';

const layout = (props) => (
    <Grid container spacing={1}>
        <Aux className="row">
            <div>
                <NavContainer />
            </div>
            <main>
                { props.children }
            </main>
        </Aux>
    </Grid>
);

export default layout;
