import React, { useState } from 'react';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Slider from '@material-ui/core/Slider';

const NavbarContainer = () => {
    const [numberOfRacers, setNumOfRacers] = useState(0);
    const [racerLevel, setRacerLevel] = useState([0]);
    const [trackDiff, setSetTrackDiff] = useState([0]);
    const [isOpen, setIsOpen] = useState(false);

    let racerAmt = 0;
    let levelRange = [0];
    let difficulty = 0;

    const SetNumOfRacers = (value) => {
        racerAmt = value;
    };

    const SetRacerLevelRange = (start, stop) => {
        levelRange = [start, stop];
    };

    const SetTrackDiff = (value) => {
        difficulty = value;
    };

    const handleDrawerOpen = () => {
        let opened = isOpen;
        setIsOpen(!opened);
    };

    const handleSumbit = () => {
        console.log(racerAmt + ", " + levelRange + ", " + difficulty);

        setNumOfRacers(racerAmt);
        setRacerLevel(levelRange);
        setSetTrackDiff(difficulty);
    };

    const expMarks = [
        {
            value: 0,
            label: 'Rookie'
        },
        {
            value: 25,
            label: 'Experienced'
        },
        {
            value: 50,
            label: 'Veteran'
        },
        {
            value: 75,
            label: 'Elite'
        },
        {
            value: 100,
            label: 'Champion'
        }
    ];

    const trackMarks = [
        {
            value: 0,
            label: 'Easy'
        },
        {
            value: 25,
            label: 'Normal'
        },
        {
            value: 50,
            label: 'Hard'
        },
        {
            value: 75,
            label: 'Elite'
        },
        {
            value: 100,
            label: 'Champion'
        }
    ];

    return(
        <div className="Navbar">
            <AppBar position="fixed">
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                    >
                        <MenuIcon/>
                    </IconButton>
                    <h1 className="Nav-Title">Racing Game</h1>
                </Toolbar>
            </AppBar>
            <Drawer
                className="Nav-Drawer-Container"
                variant="persistent"
                anchor="left"
                open={isOpen}
            >
                <div className="Nav-Drawer-Header">
                    <IconButton onClick={handleDrawerOpen}>
                        {isOpen === true ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                </div>
                <Divider />
                    <div className="Slider-Container">
                        <div className="Racer-Amt">
                            <h2>Amount</h2>
                            <Slider
                                orientation="horizontal"
                                defaultValue={0}
                                aria-labelledby="horizontal-slider"
                                valueLabelDisplay="on"
                                min={10}
                                max={42}
                                onChange={ (e, val) => SetNumOfRacers(val)}
                            />
                            <h2>Exp Level</h2>
                            <Slider
                                orientation="horizontal"
                                defaultValue={[0, 25]}
                                aria-labelledby="horizontal-slider"
                                step={25}
                                marks={expMarks}
                                onChange={(e, val) => SetRacerLevelRange([val[0], val[1]])}
                            />
                            <Divider />
                            <h2>Track Difficulty</h2>
                            <Slider
                                orientation="horizontal"
                                defaultValue={0}
                                aria-labelledby="horizontal-slider"
                                step={25}
                                marks={trackMarks}
                                onChange={(e, val) => SetTrackDiff(val)}
                            />
                    </div>
                </div>
                <Divider />
                <Button 
                    className="Race-Btn"
                    variant="outlined"
                    onClick={handleSumbit}
                    >
                        Let's Race!
                    </Button>
            </Drawer>
        </div>
    );
};

export default NavbarContainer;
